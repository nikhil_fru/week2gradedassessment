package com.hcl.week2gradedassessment;

import java.util.ArrayList;
import java.util.TreeMap;

public class SecondDataStructure {
	public void cityNameCount(ArrayList<Employee> employees) {

		TreeMap<String, Integer> cityNamesCount = new TreeMap<String, Integer>();
		for (Employee emp : employees) {
			String city = emp.getCity();
			if (cityNamesCount.containsKey(city)) {
				cityNamesCount.replace(city, ((cityNamesCount.get(city)) + 1));
			} else {
				cityNamesCount.put(city, 1);
			}
		}
		System.out.println("Count of employees from each city:");
		System.out.println(cityNamesCount);
	}

}




