
	package com.hcl.week2gradedassessment;

	import java.util.ArrayList;

	public class MainClass {

		public static void main(String[] args) {
			Employee employeeDetails1 = new Employee();
			Employee employeeDetails2 = new Employee();
			Employee employeeDetails3 = new Employee();
			Employee employeeDetails4 = new Employee();
			Employee employeeDetails5 = new Employee();
			
			employeeDetails1.setId(1);
			employeeDetails1.setName("Aman");
			employeeDetails1.setAge(20);
			employeeDetails1.setSalary(1100000);
			employeeDetails1.setDepartment("IT");
			employeeDetails1.setCity("Delhi");

			
			employeeDetails2.setId(2);
			employeeDetails2.setName("Bobby");
			employeeDetails2.setAge(22);
			employeeDetails2.setSalary(500000);
			employeeDetails2.setDepartment("HR");
			employeeDetails2.setCity("Bombay");


			employeeDetails3.setId(3);
			employeeDetails3.setName("Zoe");
			employeeDetails3.setAge(20);
			employeeDetails3.setSalary(7500000);
			employeeDetails3.setDepartment("Admin");
			employeeDetails3.setCity("Delhi");

			
			employeeDetails4.setId(4);
			employeeDetails4.setName("Smitha");
			employeeDetails4.setAge(21);
			employeeDetails4.setSalary(10000000);
			employeeDetails4.setDepartment("IT");
			employeeDetails4.setCity("Chennai");

		
			employeeDetails5.setId(5);
			employeeDetails5.setName("Smitha");
			employeeDetails5.setAge(24);
			employeeDetails5.setSalary(12000000);
			employeeDetails5.setDepartment("HR");
			employeeDetails5.setCity("Banglore");

			ArrayList<Employee> detailsOfEmployees = new ArrayList<Employee>();
			detailsOfEmployees.add(employeeDetails1);
			detailsOfEmployees.add(employeeDetails2);
			detailsOfEmployees.add(employeeDetails3);
			detailsOfEmployees.add(employeeDetails4);
			detailsOfEmployees.add(employeeDetails5);

			System.out.println("List of Employees : ");
			System.out.println("Id     Name      Age      Salary(INR)    Department   Location");
			for (Employee em : detailsOfEmployees) {
				System.out.println(em.getId() + "      " + em.getName() + "     " + em.getAge() + "      "
						+ em.getSalary() + "          " + em.getDepartment() + "          " + em.getCity());
			}
			System.out.println();
			FirstDataStructure sortedNames = new FirstDataStructure();
			sortedNames.sortingNames(detailsOfEmployees);
			SecondDataStructure employeeCount = new SecondDataStructure();
			employeeCount.cityNameCount(detailsOfEmployees);
			ThirdDataStructure salary = new ThirdDataStructure();
			salary.monthlySalary(detailsOfEmployees);

		}

	}


