package com.hcl.week2gradedassessment;

import java.util.ArrayList;
import java.util.TreeMap;

public class ThirdDataStructure {
	public void monthlySalary(ArrayList<Employee> employees) {
		try {
			TreeMap<Integer, Float> monthlySalary = new TreeMap<>();
			for (Employee e : employees) {
				monthlySalary.put(e.getId(), (float) Math.floor((float) e.getSalary() / 12));

			}
			System.out.println("Monthly salary of employees along with their id is: ");
			System.out.println(monthlySalary);
		} catch (Exception exception) {
			System.out.println("exception found " + exception.getMessage());
		}
	}

}



