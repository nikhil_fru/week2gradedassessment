package com.hcl.week2gradedassessment;
import java.util.ArrayList;
import java.util.Collections;

public class FirstDataStructure {
	public void sortingNames(ArrayList<Employee> employees) {
		ArrayList<String> empNames = new ArrayList<String>();
		for (Employee e : employees) {
			empNames.add(e.getName());
		}
		Collections.sort(empNames);
		System.out.println("Names of all the employees in sorted order are:");
		System.out.println(empNames);

	}
}


	


